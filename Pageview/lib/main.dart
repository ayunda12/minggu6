import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

//fungsi main untuk memanggil class yang pertama kali ditampilkan.
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Home(),
      // home: Scaffold(
    );
  }
}

class Home extends StatelessWidget {
  final List<String> gambar = [
    "a.jpeg",
    "b.jpg",
    "c.jpg",
    "d.jpg",
    "pp.jpg",
  ];
  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
            gradient: new LinearGradient(
                begin: FractionalOffset.topRight,
                end: FractionalOffset.bottomLeft,
                colors: [
              Colors.pink,
              Colors.blue,
            ])),
        child: new PageView.builder(
          controller: new PageController(viewportFraction: 0.8),
          itemCount: gambar.length,
          itemBuilder: (BuildContext context, int i) {
            return new Padding(
              padding:
                  new EdgeInsets.symmetric(horizontal: 5.0, vertical: 22.0),
              child: new Material(
                borderRadius: new BorderRadius.circular(15.0),
                elevation: 8.0,
                child: new Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    new Hero(
                        tag: gambar[i],
                        child: new Material(
                          child: new InkWell(
                              onTap: () => Navigator.of(context)
                                      .push(new MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        new Halamandua(
                                      gambar: gambar[i],
                                    ),
                                  )),
                              child: new Image.asset(
                                "img/${gambar[i]}",
                                fit: BoxFit.cover,
                              )),
                        ))
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class Halamandua extends StatelessWidget {
  const Halamandua({required this.gambar});
  final String gambar;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
          title: new Text("Kucing Unyu"),
          backgroundColor: Colors.grey,
          actions: <Widget>[]),
      body: new Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
                gradient: new RadialGradient(center: Alignment.center, colors: [
              Colors.yellow,
              Colors.white,
              Colors.brown.withOpacity(0.9),
            ])),
          ),
          new Center(
            child: new Hero(
              tag: gambar,
              child: new ClipOval(
                child: new SizedBox(
                  width: 200.0,
                  height: 200.0,
                  child: new Material(
                    child: new InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      child: new Image.asset("img/$gambar", fit: BoxFit.cover),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
