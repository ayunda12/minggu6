import 'package:flutter/material.dart';

//fungsi main untuk memanggil class yang pertama kali ditampilkan.
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Home(),
      // home: Scaffold(
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          //flexible disini
          Flexible(
            flex: 1,
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: <Color>[
                    Colors.red,
                    Colors.black,
                  ],
                ),
              ),
            ),
          ),
          Flexible(
            flex: 2,
            child: Container(
              decoration: BoxDecoration(
                gradient: RadialGradient(
                  radius: 2.0,
                  colors: <Color>[
                    Colors.pink,
                    Colors.purple,
                    Colors.black,
                  ],
                ),
              ),
            ),
          ),
          Flexible(
            flex: 3,
            child: Container(
              decoration: BoxDecoration(
                gradient: SweepGradient(
                  startAngle: 1.0,
                  colors: <Color>[
                    Color.fromARGB(255, 25, 111, 117),
                    Color.fromARGB(255, 158, 158, 158),
                    Color.fromARGB(255, 120, 236, 120),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
