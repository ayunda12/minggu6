import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Home());
  }
}

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String selectval = "United Kingdom";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Hewan Peliharaan"), backgroundColor: Colors.redAccent),
        body: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(50),
            child: Column(children: [
              DecoratedBox(
                  decoration: BoxDecoration(
                      color: Colors
                          .lightGreen, //background color of dropdown button
                      border: Border.all(
                          color: Colors.black38,
                          width: 3), //border of dropdown button
                      borderRadius: BorderRadius.circular(
                          50), //border raiuds of dropdown button
                      boxShadow: <BoxShadow>[
                        //apply shadow on Dropdown button
                        BoxShadow(
                            color: Color.fromRGBO(
                                0, 0, 0, 0.57), //shadow for button
                            blurRadius: 5) //blur radius of shadow
                      ]),
                  child: Padding(
                      padding: EdgeInsets.only(left: 30, right: 30),
                      child: DropdownButton(
                        value: "Kucing",
                        items: [
                          //add items in the dropdown
                          DropdownMenuItem(
                            child: Text("Kucing"),
                            value: "Kucing",
                          ),
                          DropdownMenuItem(child: Text("Ayam"), value: "Ayam"),
                          DropdownMenuItem(
                            child: Text("Kelinci"),
                            value: "Kelinci",
                          ),
                          DropdownMenuItem(
                            child: Text("Bebek"),
                            value: "Bebek",
                          )
                        ],
                        onChanged: (value) {
                          //get value when changed
                          print("You have selected $value");
                        },

                        icon: Padding(
                            //Icon at tail, arrow bottom is default icon
                            padding: EdgeInsets.only(left: 20),
                            child: Icon(Icons.arrow_circle_down_sharp)),

                        iconEnabledColor: Colors.white, //Icon color
                        style: TextStyle(
                            //te
                            color: Colors.white, //Font color
                            fontSize: 20 //font size on dropdown button
                            ),

                        dropdownColor:
                            Colors.redAccent, //dropdown background color
                        underline: Container(), //remove underline
                        isExpanded: true, //make true to make width 100%
                      ))),
              Divider(),
            ])));
  }
}
